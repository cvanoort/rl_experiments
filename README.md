# rl_experiments

Small projects for learning how to implement reinforcement learning algorithms using Keras and TensorFlow.

# To Do
- Implement prioritized experience replay
- Implement (Advantage) Actor-Critic
- Implement concepts from The Reactor (https://arxiv.org/abs/1704.04651)
- Investigate when to stack observation sequences
- Implement Proximal Policy Gradients
- Implement curiosity driven learning (https://pathak22.github.io/noreward-rl/resources/icml17.pdf)
- Implement some algorithms from http://spinningup.openai.com/en/latest/user/algorithms.html#what-s-included
- Implement an agent that uses a world model (https://arxiv.org/abs/1803.10122)
- Work on some of the goodies shown in http://spinningup.openai.com/en/latest/index.html