"""
Implements an Actor-Critic agent that operates on OpenAI Gym environments.

Reference:
    https://simoninithomas.github.io/Deep_reinforcement_learning_Course/
"""

import gym
import keras.backend as K
import numpy as np
from keras.layers import Activation, BatchNormalization, concatenate, Dense, Input, Lambda
from keras.models import Model, clone_model
from keras.optimizers import Adam
from keras.utils import to_categorical
from sortedcontainers import SortedList


def policy_gradient_loss(discounted_rewards, neg_log_prob):
    return K.mean(discounted_rewards * neg_log_prob, axis=-1)


def normalize(arr, eps=1e-6):
    return (arr - np.mean(arr)) / (np.std(arr) + eps)


def exploration_strategy(max_prob, min_prob, decay_rate):
    def get_exploration_prob(step):
        return min_prob + (max_prob - min_prob) * np.exp(-decay_rate * step)
    return get_exploration_prob


class ExperienceBuffer:
    """
    Buffer to be used for experience replay in RL.
    Maintains a uniform random sample of all added experiences following the
    technique described in https://arxiv.org/abs/1811.11682
    """
    def __init__(self, max_size):
        self.max_size = max_size
        self.buffer = SortedList()

    def add(self, experience):
        self.buffer.add((np.random.random(), experience))
        if len(self.buffer) > self.max_size:
            self.buffer.pop()

    def sample(self, batch_size):
        buffer_size = len(self.buffer)
        index = np.random.choice(
            np.arange(buffer_size),
            size=batch_size,
            replace=False,
        )
        return [self.buffer[i][1] for i in index]

    def fill_buffer(self, env, steps=None, render=False):
        if steps is None:
            steps = self.max_size

        state = env.reset()
        for i in range(steps):
            action = env.action_space.sample()
            next_state, reward, done, _ = env.step(action)

            if render:
                env.render()

            if done:
                next_state = np.zeros(state.shape)
                self.add((state, action, reward, next_state, done))
                state = env.reset()

            else:
                self.add((state, action, reward, next_state, done))
                state = next_state


class ActorCriticAgent:
    """
    Implements a learning agent that leverages the Actor-Critic method.

    Features:

    """

    def __init__(self, env, tau=10, gamma=0.9, buffer_size=10**6, batch_size=256):
        self.action_space = env.action_space
        self.obs_space = env.observation_space

        self.tau = tau
        self.gamma = gamma
        self.update_count = 0
        self.batch_size = batch_size

        self.experience_buffer = ExperienceBuffer(buffer_size)
        self.experience_buffer.fill_buffer(env, steps=batch_size)

        self.actor_train, self.actor_predict = self.build_actor_models()
        self.actor_train_target = clone_model(self.actor_train)
        self.actor_predict_target = clone_model(self.actor_predict)

        self.critic_train, self.critic_predict = self.build_critic_models()
        self.critic_train_target = clone_model(self.critic_train)
        self.critic_predict_target = clone_model(self.critic_predict)

    def build_actor_models(self, neurons=20):
        obs = Input(shape=self.obs_space.shape)
        actions = Input(shape=[self.action_space.n])

        pred = Dense(neurons)(obs)
        pred = BatchNormalization()(pred)
        pred = Activation('elu')(pred)

        pred = Dense(neurons)(pred)
        pred = BatchNormalization()(pred)
        pred = Activation('elu')(pred)

        pred = concatenate([obs, pred])

        pred = Dense(self.action_space.n)(pred)
        pred = BatchNormalization()(pred)
        pred_actions = Activation('softmax')(pred)

        neg_log_prob = Lambda(
            lambda x: -K.log(K.sum(x[0] * x[1], axis=-1, keepdims=True)),
            output_shape=[1],
        )([pred_actions, actions])

        train_model = Model(inputs=[obs, actions], outputs=[neg_log_prob])
        train_model.compile(optimizer=Adam(), loss=policy_gradient_loss)

        predict_model = Model(inputs=[obs], outputs=[pred_actions])
        predict_model.compile(optimizer=Adam(), loss=policy_gradient_loss)

        return train_model, predict_model

    def build_critic_models(self, neurons=20):
        obs = Input(shape=self.obs_space.shape)
        actions = Input(shape=[self.action_space.n])

        # Shared component
        pred = Dense(neurons)(obs)
        pred = BatchNormalization()(pred)
        pred = Activation('elu')(pred)

        pred = Dense(neurons)(pred)
        pred = BatchNormalization()(pred)
        pred = Activation('elu')(pred)

        pred = concatenate([obs, pred])

        # Value branch
        value_pred = Dense(neurons)(pred)
        value_pred = BatchNormalization()(value_pred)
        value_pred = Activation('elu')(value_pred)
        value_pred = Dense(1)(value_pred)

        # Advantage branch
        advantage_pred = Dense(neurons)(pred)
        advantage_pred = BatchNormalization()(advantage_pred)
        advantage_pred = Activation('elu')(advantage_pred)
        advantage_pred = Dense(self.action_space.n)(advantage_pred)

        # Combine value branch and advantage branch
        pred_qs = Lambda(
            lambda x: x[0] + (x[1] - K.mean(x[1], axis=-1, keepdims=True))
        )([value_pred, advantage_pred])

        # Select an action
        pred_q = Lambda(
            lambda x: K.sum(x[0] * x[1], axis=-1, keepdims=True)
        )([pred_qs, actions])

        # Compile models
        train_model = Model(inputs=[obs, actions], outputs=[pred_q])
        train_model.compile(optimizer='adam', loss='mse')

        predict_model = Model(inputs=[obs], outputs=[pred_qs])
        predict_model.compile(optimizer='adam', loss='mse')

        return train_model, predict_model

    def update(self):
        batch = self.experience_buffer.sample(self.batch_size)
        states = np.array([x[0] for x in batch])
        actions = np.array([x[1] for x in batch])
        actions_cat = to_categorical(actions, num_classes=self.action_space.n)
        rewards = np.array([x[2] for x in batch])
        next_states = np.array([x[3] for x in batch])
        dones = np.array([x[4] for x in batch])

        state_qs = self.get_qs(states)[np.arange(len(states)), actions]
        next_actions = self.get_actions(next_states)
        next_state_qs = self.get_qs(next_states)[np.arange(len(states)), next_actions]

        target_qs = np.where(
            dones,
            rewards,
            rewards + self.gamma * next_state_qs
        )

        actor_loss = self.actor_train.train_on_batch([states, actions_cat], state_qs)
        critic_loss = self.critic_train.train_on_batch([states, actions_cat], target_qs)

        if self.update_count % self.tau:
            self.actor_predict_target.set_weights(self.actor_predict.get_weights())
            self.critic_predict_target.set_weights(self.critic_predict.get_weights())
        self.update_count += 1
        return actor_loss + critic_loss

    def get_qs(self, obs):
        if len(obs.shape) == 1:
            obs = obs[None, ...]
        return self.critic_predict_target.predict(obs)

    def get_action(self, obs):
        if len(obs.shape) == 1:
            obs = obs[None, ...]

        probs = self.actor_predict.predict(obs).flatten()
        return np.random.choice(len(probs), p=probs)

    def get_actions(self, obs):
        return np.apply_along_axis(
            lambda x: np.random.choice(len(x), p=x),
            axis=1,
            arr=self.actor_predict.predict(obs)
        )

    def add_experience(self, experience):
        self.experience_buffer.add(experience)


def main(
        episodes=1500,
        max_steps=500,
        gamma=0.95,
        tau=10,
        batch_size=512,
        env_name='CartPole-v0',
        render=False,
):
    env = gym.make(env_name)
    agent = ActorCriticAgent(env=env, tau=tau, gamma=gamma, batch_size=batch_size)
    reward_list = []

    for i in range(episodes):
        episode_rewards = []
        state = env.reset()

        for j in range(max_steps):
            action = agent.get_action(state)
            next_state, reward, done, _ = env.step(action)
            episode_rewards.append(reward)

            if render:
                env.render()

            if done:
                next_state = np.zeros(state.shape, dtype=np.int)
                reward_list.append(np.sum(episode_rewards))
                agent.experience_buffer.add((state, action, reward, next_state, done))
                break

            else:
                agent.experience_buffer.add((state, action, reward, next_state, done))
                state = next_state

        loss = agent.update()

        print(
            f'Episode: {i}',
            f'\n\tTotal reward:  {reward_list[-1]}',
            f'\n\tTraining loss: {loss:.4f}',
        )
    for episode in range(1):
        total_rewards = 0
        state = env.reset()

        print("****************************************************")
        print(f"EPISODE {episode}:")

        while True:
            action = agent.get_action(state)
            next_state, reward, done, _ = env.step(action)
            env.render()
            total_rewards += reward

            if done:
                print(f"Score: {total_rewards}")
                break

            state = next_state
    env.close()


if __name__ == '__main__':
    main()
