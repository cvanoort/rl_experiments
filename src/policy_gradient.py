"""
Implements a Policy Gradient agent that operates on OpenAI Gym environments.

Reference:
    https://simoninithomas.github.io/Deep_reinforcement_learning_Course/
"""


import gym
import keras.backend as K
import numpy as np
from keras.layers import Activation, BatchNormalization, concatenate, Dense, Input, Lambda
from keras.optimizers import Adam
from keras.models import Model
from keras.utils import to_categorical


def policy_gradient_loss(discounted_rewards, neg_log_prob):
    return K.mean(discounted_rewards * neg_log_prob, axis=-1)


def normalize(arr, eps=1e-6):
    return (arr - np.mean(arr)) / (np.std(arr) + eps)


class PolicyGradientAgent:
    """
    Implements a learning agent that leverages vanilla policy gradient methods.

    Features:
        - Simple residual architecture with dense layers
        - Vanilla PG implementation
    """
    def __init__(self, env, gamma=0.9):
        self.action_space = env.action_space
        self.obs_space = env.observation_space
        self.gamma = gamma

        self.train_model, self.predict_model = self.build_model()

    def build_model(self, neurons=20):
        obs = Input(shape=self.obs_space.shape)
        actions = Input(shape=[self.action_space.n])

        pred = Dense(neurons)(obs)
        pred = BatchNormalization()(pred)
        pred = Activation('elu')(pred)

        pred = Dense(neurons)(pred)
        pred = BatchNormalization()(pred)
        pred = Activation('elu')(pred)

        pred = concatenate([obs, pred])

        pred = Dense(self.action_space.n)(pred)
        pred = BatchNormalization()(pred)
        pred_actions = Activation('softmax')(pred)

        neg_log_prob = Lambda(
            lambda x: -K.log(K.sum(x[0] * x[1], axis=-1, keepdims=True)),
            output_shape=[1],
        )([pred_actions, actions])

        train_model = Model(inputs=[obs, actions], outputs=[neg_log_prob])
        train_model.compile(optimizer=Adam(), loss=policy_gradient_loss)

        predict_model = Model(inputs=[obs], outputs=[pred_actions])
        predict_model.compile(optimizer=Adam(), loss=policy_gradient_loss)

        return train_model, predict_model

    def update(self, obs, actions, rewards):
        obs = np.vstack(obs)
        actions = to_categorical(actions, self.action_space.n)
        rewards = [self.process_rewards(r) for r in rewards]
        rewards = np.hstack(rewards)[..., np.newaxis]
        return self.train_model.train_on_batch([obs, actions], rewards)

    def get_action(self, obs):
        if len(obs.shape) == 1:
            obs = obs[None, ...]

        probs = self.predict_model.predict(obs).flatten()
        return np.random.choice(len(probs), p=probs)

    def get_actions(self, obs):
        return np.apply_along_axis(
            lambda x: np.random.choice(len(x), p=x),
            axis=1,
            arr=self.predict_model.predict(obs)
        )

    def process_rewards(self, rewards):
        """
        Calculates the discounted cumulative rewards experienced over an episode,
        then normalizes by subtracting the mean and dividing by the std. dev.

        Args:
            rewards: list[float], Sequence of rewards experienced over an episode.

        Returns: numpy.ndarray
            Normalized, discounted, cumulative rewards.
        """
        discounted_rewards = np.zeros_like(rewards)
        acc = 0
        for t in reversed(range(len(rewards))):
            acc = acc * self.gamma + rewards[t]
            discounted_rewards[t] = acc
        return normalize(discounted_rewards)


def main(
        updates=500,
        max_steps=500,
        gamma=0.95,
        env_name='CartPole-v0',
        render=False,
):
    env = gym.make(env_name)
    agent = PolicyGradientAgent(env=env, gamma=gamma)
    summed_rewards = []
    episode_count = 0
    samples_per_update = np.linspace(1, 30, num=updates).astype(int)

    for count in samples_per_update:
        states, actions, rewards = [], [], []
        for j in range(count):
            rewards.append([])
            state = env.reset()
            for k in range(max_steps):
                states.append(state)

                action = agent.get_action(state)
                actions.append(action)

                next_state, reward, done, _ = env.step(action)
                rewards[j].append(reward)

                if render:
                    env.render()

                if done:
                    episode_count += 1
                    summed_rewards.append(np.sum(rewards[j]))
                    print(
                        f'Episode: {episode_count}',
                        f'\n\tCumulative reward:   {summed_rewards[-1]:.4f}',
                        f'\n\tAverage Cum. reward: {np.mean(summed_rewards):.4f}',
                        f'\n\tMax Cum. reward:     {np.max(summed_rewards):.4f}',
                    )
                    break

                else:
                    state = next_state
        loss = agent.update(states, actions, rewards)
        print(f'\n\tTraining loss:       {loss:.4f}',)

    for episode in range(1):
        total_rewards = 0
        state = env.reset()

        print("****************************************************")
        print(f"EPISODE {episode}:")

        while True:
            action = agent.get_action(state)
            next_state, reward, done, _ = env.step(action)
            env.render()
            total_rewards += reward

            if done:
                print(f"Score: {total_rewards}")
                break

            state = next_state
    env.close()


if __name__ == '__main__':
    main(env_name='CartPole-v1')
