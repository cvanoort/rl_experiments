"""
Implements a DQN agent that operates on OpenAI Gym environments.

Reference:
    https://simoninithomas.github.io/Deep_reinforcement_learning_Course/
"""


import gym
import keras.backend as K
import numpy as np
from keras.layers import Activation, BatchNormalization, concatenate, Dense, Input, Lambda
from keras.models import Model, clone_model
from keras.utils import to_categorical
from sortedcontainers import SortedList


def exploration_strategy(max_prob, min_prob, decay_rate):
    def get_exploration_prob(step):
        return min_prob + (max_prob - min_prob) * np.exp(-decay_rate * step)
    return get_exploration_prob


class ExperienceBuffer:
    """
    Buffer to be used for experience replay in RL.
    Maintains a uniform random sample of all added experiences following the
    technique described in https://arxiv.org/abs/1811.11682
    """
    def __init__(self, max_size):
        self.max_size = max_size
        self.buffer = SortedList()

    def add(self, experience):
        self.buffer.add((np.random.random(), experience))
        if len(self.buffer) > self.max_size:
            self.buffer.pop()

    def sample(self, batch_size):
        buffer_size = len(self.buffer)
        index = np.random.choice(
            np.arange(buffer_size),
            size=batch_size,
            replace=False,
        )
        return [self.buffer[i][1] for i in index]

    def fill_buffer(self, env, steps=None, render=False):
        if steps is None:
            steps = self.max_size

        state = env.reset()
        for i in range(steps):
            action = env.action_space.sample()
            next_state, reward, done, _ = env.step(action)

            if render:
                env.render()

            if done:
                next_state = np.zeros(state.shape)
                self.add((state, action, reward, next_state, done))
                state = env.reset()

            else:
                self.add((state, action, reward, next_state, done))
                state = next_state


class DQNAgent:
    """
    Implements a learning agent that leverages naive Deep Q-Learning methods.

    Features:
        - Simple residual architecture with dense layers
        - Vanilla DQN implementation
        - Experience replay buffer with uniform experience sampling.
    """
    def __init__(self, env, gamma=0.9, buffer_size=10**6, batch_size=256):
        self.action_space = env.action_space
        self.obs_space = env.observation_space

        self.train_model, self.predict_model = self.build_dqn()

        self.gamma = gamma
        self.update_count = 0
        self.batch_size = batch_size

        self.experience_buffer = ExperienceBuffer(buffer_size)
        self.experience_buffer.fill_buffer(env, steps=batch_size)

    def build_dqn(self, neurons=20):
        obs = Input(shape=self.obs_space.shape)
        actions = Input(shape=[self.action_space.n])

        pred = Dense(neurons)(obs)
        pred = BatchNormalization()(pred)
        pred = Activation('elu')(pred)

        pred = Dense(neurons)(pred)
        pred = BatchNormalization()(pred)
        pred = Activation('elu')(pred)

        pred = concatenate([obs, pred])

        pred_out = Dense(self.action_space.n)(pred)

        train_out = Lambda(
            lambda x: K.sum(x[0] * x[1], axis=-1, keepdims=True)
        )([pred_out, actions])

        train_model = Model(inputs=[obs, actions], outputs=[train_out])
        train_model.compile(optimizer='adam', loss='mse')

        predict_model = Model(inputs=[obs], outputs=[pred_out])
        predict_model.compile(optimizer='adam', loss='mse')

        return train_model, predict_model

    def update(self):
        batch = self.experience_buffer.sample(self.batch_size)
        states = np.array([x[0] for x in batch])
        actions = to_categorical(np.array([x[1] for x in batch]), num_classes=self.action_space.n)
        rewards = np.array([x[2] for x in batch])
        next_states = np.array([x[3] for x in batch])
        dones = np.array([x[4] for x in batch])

        target_qs = []
        next_state_qs = self.get_qs(next_states)
        next_actions = self.get_actions(next_states)
        for j in range(self.batch_size):
            if dones[j]:
                target_qs.append(rewards[j])
            else:
                target_qs.append(
                    rewards[j] + self.gamma * next_state_qs[j][next_actions[j]]
                )
        target_qs = np.array(target_qs)

        loss = self.train_model.train_on_batch(x=[states, actions], y=target_qs)

        self.update_count += 1
        return loss

    def get_qs(self, obs):
        if len(obs.shape) == 1:
            obs = obs[None, ...]
        return self.predict_model.predict(obs)

    def get_action(self, obs, epsilon=0.):
        if len(obs.shape) == 1:
            obs = obs[None, ...]

        if np.random.random() < epsilon:
            return self.action_space.sample()
        else:
            return np.argmax(self.predict_model.predict(obs), axis=-1)[0]

    def get_actions(self, obs, epsilon=0.):
        random_actions = np.random.choice(self.action_space.n, size=len(obs))
        selected_actions = np.argmax(self.predict_model.predict(obs), axis=-1).flatten()
        return np.where(
            np.random.random(size=len(obs)) < epsilon,
            random_actions,
            selected_actions,
        )

    def add_experience(self, experience):
        self.experience_buffer.add(experience)


class DoubleDQNAgent:
    """
    Implements a learning agent that leverages Deep Q-Learning methods.

    Features:
        - Experience replay buffer with uniform sampling.
        - Frozen target network, updated every tau training batches.
        - Target Q-values are calculated using a prediction net and a target net
            - Prediction net selects the action to perform.
            - Target net evaluates the (state, action) tuple.
    """
    def __init__(self, env, tau=10, gamma=0.9, buffer_size=10**6, batch_size=256):
        self.action_space = env.action_space
        self.obs_space = env.observation_space

        self.train_model, self.predict_model = self.build_dqn()
        self.target_network = clone_model(self.predict_model)

        self.tau = tau
        self.gamma = gamma
        self.update_count = 0
        self.batch_size = batch_size

        self.experience_buffer = ExperienceBuffer(buffer_size)
        self.experience_buffer.fill_buffer(env, steps=batch_size)

    def build_dqn(self, neurons=20):
        obs = Input(shape=self.obs_space.shape)
        actions = Input(shape=[self.action_space.n])

        pred = Dense(neurons)(obs)
        pred = BatchNormalization()(pred)
        pred = Activation('elu')(pred)

        pred = Dense(neurons)(pred)
        pred = BatchNormalization()(pred)
        pred = Activation('elu')(pred)

        pred = concatenate([obs, pred])

        pred_out = Dense(self.action_space.n)(pred)

        train_out = Lambda(
            lambda x: K.sum(x[0] * x[1], axis=-1, keepdims=True)
        )([pred_out, actions])

        train_model = Model(inputs=[obs, actions], outputs=[train_out])
        train_model.compile(optimizer='adam', loss='mse')

        predict_model = Model(inputs=[obs], outputs=[pred_out])
        predict_model.compile(optimizer='adam', loss='mse')

        return train_model, predict_model

    def update(self):
        batch = self.experience_buffer.sample(self.batch_size)
        states = np.array([x[0] for x in batch])
        actions = to_categorical(np.array([x[1] for x in batch]), num_classes=self.action_space.n)
        rewards = np.array([x[2] for x in batch])
        next_states = np.array([x[3] for x in batch])
        dones = np.array([x[4] for x in batch])

        target_qs = []
        next_state_qs = self.get_qs(next_states)
        next_actions = self.get_actions(next_states)
        for j in range(self.batch_size):
            if dones[j]:
                target_qs.append(rewards[j])
            else:
                target_qs.append(
                    rewards[j] + self.gamma * next_state_qs[j][next_actions[j]]
                )
        target_qs = np.array(target_qs)

        loss = self.train_model.train_on_batch(x=[states, actions], y=target_qs)

        if self.update_count % self.tau:
            self.target_network.set_weights(self.predict_model.get_weights())
        self.update_count += 1
        return loss

    def get_qs(self, obs):
        if len(obs.shape) == 1:
            obs = obs[None, ...]
        return self.target_network.predict(obs)

    def get_action(self, obs, epsilon=0.):
        if len(obs.shape) == 1:
            obs = obs[None, ...]

        if np.random.random() < epsilon:
            return self.action_space.sample()
        else:
            return np.argmax(self.predict_model.predict(obs), axis=-1)[0]

    def get_actions(self, obs, epsilon=0.):
        random_actions = np.random.choice(self.action_space.n, size=len(obs))
        selected_actions = np.argmax(self.predict_model.predict(obs), axis=-1).flatten()
        return np.where(
            np.random.random(size=len(obs)) < epsilon,
            random_actions,
            selected_actions,
        )

    def add_experience(self, experience):
        self.experience_buffer.add(experience)


class DuelingDQNAgent:
    """
    Implements a learning agent that leverages Deep Q-Learning methods.

    Features:
        - Experience replay buffer with uniform sampling.
        - Frozen target network, updated every tau training batches.
        - Target Q-values are calculated using a prediction net and target net
            - Prediction net selects the action to perform.
            - Target net evaluates the (state, action) tuple.
        - Utilizes the Dueling DQN architecture, which separates the state value
            estimation from the action advantage estimation using independent
            network branches that are later joined to form a Q-value.
    """

    def __init__(self, env, tau=10, gamma=0.9, buffer_size=10**7, batch_size=256):
        self.action_space = env.action_space
        self.obs_space = env.observation_space

        self.train_model, self.predict_model = self.build_models()
        self.target_network = clone_model(self.predict_model)

        self.tau = tau
        self.gamma = gamma
        self.update_count = 0
        self.batch_size = batch_size

        self.experience_buffer = ExperienceBuffer(buffer_size)
        self.experience_buffer.fill_buffer(env, steps=batch_size)

    def build_models(self, neurons=20):
        obs = Input(shape=self.obs_space.shape)
        actions = Input(shape=[self.action_space.n])

        # Shared component
        pred = Dense(neurons)(obs)
        pred = BatchNormalization()(pred)
        pred = Activation('elu')(pred)

        pred = Dense(neurons)(pred)
        pred = BatchNormalization()(pred)
        pred = Activation('elu')(pred)

        pred = concatenate([obs, pred])

        # State value branch
        value_pred = Dense(neurons)(pred)
        value_pred = BatchNormalization()(value_pred)
        value_pred = Activation('elu')(value_pred)
        value_pred = Dense(1)(value_pred)

        # Action advantage branch
        advantage_pred = Dense(neurons)(pred)
        advantage_pred = BatchNormalization()(advantage_pred)
        advantage_pred = Activation('elu')(advantage_pred)
        advantage_pred = Dense(self.action_space.n)(advantage_pred)

        # Combine value branch and advantage branch
        pred_qs = Lambda(
            lambda x: x[0] + (x[1] - K.mean(x[1], axis=-1, keepdims=True))
        )([value_pred, advantage_pred])

        # Select an action
        pred_q = Lambda(
            lambda x: K.sum(x[0] * x[1], axis=-1, keepdims=True)
        )([pred_qs, actions])

        # Compile models
        train_model = Model(inputs=[obs, actions], outputs=[pred_q])
        train_model.compile(optimizer='adam', loss='mse')

        predict_model = Model(inputs=[obs], outputs=[pred_qs])
        predict_model.compile(optimizer='adam', loss='mse')

        return train_model, predict_model

    def update(self):
        batch = self.experience_buffer.sample(self.batch_size)
        states = np.array([x[0] for x in batch])
        actions = np.array([x[1] for x in batch])
        actions_cat = to_categorical(actions, num_classes=self.action_space.n)
        rewards = np.array([x[2] for x in batch])
        next_states = np.array([x[3] for x in batch])
        dones = np.array([x[4] for x in batch])

        next_actions = self.get_actions(next_states)
        next_state_qs = self.get_qs(next_states)[np.arange(len(states)), next_actions]

        target_qs = np.where(
            dones,
            rewards,
            rewards + self.gamma * next_state_qs
        )

        loss = self.train_model.train_on_batch(x=[states, actions_cat], y=target_qs)

        if self.update_count % self.tau:
            self.target_network.set_weights(self.predict_model.get_weights())
        self.update_count += 1
        return loss

    def get_qs(self, obs):
        if len(obs.shape) == 1:
            obs = obs[None, ...]
        return self.target_network.predict(obs)

    def get_action(self, obs, epsilon=0.):
        if len(obs.shape) == 1:
            obs = obs[None, ...]

        if np.random.random() < epsilon:
            return self.action_space.sample()
        else:
            return np.argmax(self.predict_model.predict(obs), axis=-1)[0]

    def get_actions(self, obs, epsilon=0.):
        random_actions = np.random.choice(self.action_space.n, size=len(obs))
        selected_actions = np.argmax(self.predict_model.predict(obs), axis=-1).flatten()
        return np.where(
            np.random.random(size=len(obs)) < epsilon,
            random_actions,
            selected_actions,
        )

    def add_experience(self, experience):
        self.experience_buffer.add(experience)


def main(
        episodes=1500,
        max_steps=500,
        max_epsilon=1.,
        min_epsilon=1e-6,
        eps_decay=1e-3,
        gamma=0.95,
        tau=10,
        batch_size=512,
        env_name='CartPole-v0',
        render=False,
):
    env = gym.make(env_name)
    agent = DuelingDQNAgent(env=env, tau=tau, gamma=gamma, batch_size=batch_size)
    reward_list = []
    get_explore_prob = exploration_strategy(max_epsilon, min_epsilon, eps_decay)
    exploration_decay_step = 0
    epsilon = max_epsilon

    for i in range(episodes):
        episode_rewards = []
        state = env.reset()

        for j in range(max_steps):
            epsilon = get_explore_prob(exploration_decay_step)
            exploration_decay_step += 1

            action = agent.get_action(state, epsilon=epsilon)
            next_state, reward, done, _ = env.step(action)
            episode_rewards.append(reward)

            if render:
                env.render()

            if done:
                next_state = np.zeros(state.shape, dtype=np.int)
                reward_list.append(np.sum(episode_rewards))
                agent.experience_buffer.add((state, action, reward, next_state, done))
                break

            else:
                agent.experience_buffer.add((state, action, reward, next_state, done))
                state = next_state

        loss = agent.update()

        print(
            f'Episode: {i}',
            f'\n\tExplore P:     {epsilon:.4f}',
            f'\n\tTotal reward:  {reward_list[-1]}',
            f'\n\tTraining loss: {loss:.4f}',
        )
    for episode in range(1):
        total_rewards = 0
        state = env.reset()

        print("****************************************************")
        print(f"EPISODE {episode}:")

        while True:
            action = agent.get_action(state)
            next_state, reward, done, _ = env.step(action)
            env.render()
            total_rewards += reward

            if done:
                print(f"Score: {total_rewards}")
                break

            state = next_state
    env.close()


if __name__ == '__main__':
    main(env_name='CartPole-v1')
